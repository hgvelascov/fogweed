from flask import Flask, render_template, request, redirect, url_for
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime

app = Flask(__name__)

# Conectar MongoDB
client = MongoClient('mongodb://localhost:27017/')
db = client['Fogweed']  
coleccionPlantas = db['Plantas']  # Reemplaza con el nombre de tu colección

@app.route('/')
def index():
    # Filtrar los documentos donde Germinación.SemillaVive es True
    plantas = coleccionPlantas.find({'Germinación.SemillaVive': True})
    return render_template("index.html", plantas = plantas)

@app.route('/update', methods=['POST'])
def update():

    ids = request.form.getlist('id')

    ###################### Nutrición ##########################
    nutriente = request.form.get('nutriente')
    cantidad = request.form.get('cantidad')
    concentracion = request.form.get('concentracion')
    fase = request.form.get('fase')

    ######################## Poda #############################
    poda = request.form.get('poda')

    ###################### Trasplante #########################
    CapacidadMatera = request.form.get('CapacidadMatera')
    TipoMatera = request.form.get('TipoMatera')

    ####################### Esquejes ##########################

    esqueje_id = str(ObjectId())

    nuevo_plan_riego = {}
    nueva_poda = {}
    nuevo_trasplante = {}

    ######################################################################## Nutrición #####################################################################
    if nutriente or cantidad or concentracion:
        nuevo_plan_riego[datetime.now().isoformat()] = {
            'Nutriente': nutriente if nutriente else '',
            'Cantidad': cantidad if cantidad else '',
            'Concentración': concentracion if concentracion else ''
        }

    if nuevo_plan_riego:
        for id in ids:
            if 'PlanRiego' not in coleccionPlantas.find_one({'_id': ObjectId(id)})[f'{fase}'].keys():
                coleccionPlantas.update_one(
                {'_id': ObjectId(id)},
                 {'$set': {f'{fase}.PlanRiego': {}}}
            )

            coleccionPlantas.update_one(
                {'_id': ObjectId(id)},
                {'$set': {f'{fase}.PlanRiego': {**coleccionPlantas.find_one({'_id': ObjectId(id)})[f'{fase}']['PlanRiego'], **nuevo_plan_riego}}}
            )

    ######################################################################### Poda #########################################################################
    if poda:
        nueva_poda[datetime.now().isoformat()] = poda
    
    if nueva_poda:
        for id in ids:
            coleccionPlantas.update_one(
                {'_id': ObjectId(id)},
                {'$set': {f'{fase}.Podas': {**coleccionPlantas.find_one({'_id': ObjectId(id)})[f'{fase}']['Podas'], **nueva_poda}}}
            )

    ####################################################################### Trasplante #######################################################################
    if CapacidadMatera or TipoMatera:
        nuevo_trasplante[datetime.now().isoformat()] = {
            'CapacidadMatera': CapacidadMatera if CapacidadMatera else '',
            'TipoMatera': TipoMatera if TipoMatera else ''
        }
    
    if nuevo_trasplante:
        for id in ids:
            coleccionPlantas.update_one(
                {'_id': ObjectId(id)},
                {'$set': {f'{fase}.Trasplante': {**coleccionPlantas.find_one({'_id': ObjectId(id)})[f'{fase}']['Trasplante'], **nuevo_trasplante}}}
            )

    return redirect(url_for('index'))


@app.route('/registrar_nueva_planta', methods=['POST'])
def registrar_nueva_planta():

    ####################################################################### Esqueje #######################################################################
    ids = request.form.getlist('id')
    for id in ids:

        esqueje_id =ObjectId()

        VegetacionObjects = coleccionPlantas.find_one({'_id': ObjectId(id)})['Vegetación']
        if 'Esquejes' in list(VegetacionObjects.keys()):

            coleccionPlantas.update_one(
                {'_id': ObjectId(id)},
                {'$set': {'Vegetación.Esquejes': {**coleccionPlantas.find_one({'_id': ObjectId(id)})['Vegetación']['Esquejes'], **{datetime.now().isoformat():  {'Esqueje_id': str(esqueje_id)}}}}}
            )
        else:
            coleccionPlantas.update_one(
                {'_id': ObjectId(id)},
                {'$set': {'Vegetación.Esquejes': {datetime.now().isoformat():  {'Esqueje_id': str(esqueje_id)}}}}
            )

        nueva_planta = {
            '_id': esqueje_id,
            'Variedad': coleccionPlantas.find_one({'_id': ObjectId(id)})['Variedad'],
            'Germinación': {
                'Método': 'Esqueje',
                'Madre_id': id,
                'FechaInicio': datetime.now().isoformat(),
                'Vive': True,
                'FechaSiembra': datetime.now().isoformat()
            },
            'Vegetación': {
                'FechaCotiledonExpuesto': '',
                'FechaPrimeraHoja': '',
                'PlanRiego': {},
                'Podas': {},
                'Trasplante': {},
                'Esquejes': {}
            }
        }

        coleccionPlantas.insert_one(nueva_planta)

    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)